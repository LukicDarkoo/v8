# This file is generated by gyp; do not edit.

TOOLSET := host
TARGET := v8_base
DEFS_Debug := \
	'-DV8_TARGET_ARCH_ARM' \
	'-DCAN_USE_ARMV7_INSTRUCTIONS' \
	'-DV8_DEPRECATION_WARNINGS' \
	'-DV8_I18N_SUPPORT' \
	'-DICU_UTIL_DATA_IMPL=ICU_UTIL_DATA_STATIC' \
	'-DU_USING_ICU_NAMESPACE=0' \
	'-DU_ENABLE_DYLOAD=0' \
	'-DU_STATIC_IMPLEMENTATION' \
	'-DUSE_EABI_HARDFLOAT=1' \
	'-DENABLE_DISASSEMBLER' \
	'-DV8_ENABLE_CHECKS' \
	'-DOBJECT_PRINT' \
	'-DVERIFY_HEAP' \
	'-DDEBUG' \
	'-DTRACE_MAPS' \
	'-DENABLE_EXTRA_CHECKS' \
	'-DENABLE_HANDLE_ZAPPING'

# Flags passed to all source files.
CFLAGS_Debug := \
	-Wall \
	 \
	-W \
	-Wno-unused-parameter \
	-Wno-long-long \
	-pthread \
	-fno-exceptions \
	-pedantic \
	-Wno-missing-field-initializers \
	-m32 \
	-m32 \
	-g \
	-O0 \
	-Woverloaded-virtual \
	-Woverloaded-virtual \
	-fdata-sections \
	-ffunction-sections \
	-fdata-sections \
	-ffunction-sections

# Flags passed to only C files.
CFLAGS_C_Debug :=

# Flags passed to only C++ files.
CFLAGS_CC_Debug := \
	-Wnon-virtual-dtor \
	-fno-rtti \
	-std=gnu++0x

INCS_Debug := \
	-I$(srcdir)/. \
	-I$(srcdir)/third_party/icu/source/i18n \
	-I$(srcdir)/third_party/icu/source/common

DEFS_Optdebug := \
	'-DV8_TARGET_ARCH_ARM' \
	'-DCAN_USE_ARMV7_INSTRUCTIONS' \
	'-DV8_DEPRECATION_WARNINGS' \
	'-DV8_I18N_SUPPORT' \
	'-DICU_UTIL_DATA_IMPL=ICU_UTIL_DATA_STATIC' \
	'-DU_USING_ICU_NAMESPACE=0' \
	'-DU_ENABLE_DYLOAD=0' \
	'-DU_STATIC_IMPLEMENTATION' \
	'-DUSE_EABI_HARDFLOAT=1' \
	'-DENABLE_DISASSEMBLER' \
	'-DV8_ENABLE_CHECKS' \
	'-DOBJECT_PRINT' \
	'-DVERIFY_HEAP' \
	'-DDEBUG' \
	'-DTRACE_MAPS' \
	'-DENABLE_EXTRA_CHECKS' \
	'-DENABLE_HANDLE_ZAPPING' \
	'-DOPTIMIZED_DEBUG'

# Flags passed to all source files.
CFLAGS_Optdebug := \
	-Wall \
	 \
	-W \
	-Wno-unused-parameter \
	-Wno-long-long \
	-pthread \
	-fno-exceptions \
	-pedantic \
	-Wno-missing-field-initializers \
	-m32 \
	-m32 \
	-g \
	-Woverloaded-virtual \
	-Woverloaded-virtual \
	-fdata-sections \
	-ffunction-sections \
	-O3 \
	-fdata-sections \
	-ffunction-sections \
	-O3

# Flags passed to only C files.
CFLAGS_C_Optdebug :=

# Flags passed to only C++ files.
CFLAGS_CC_Optdebug := \
	-Wnon-virtual-dtor \
	-fno-rtti \
	-std=gnu++0x

INCS_Optdebug := \
	-I$(srcdir)/. \
	-I$(srcdir)/third_party/icu/source/i18n \
	-I$(srcdir)/third_party/icu/source/common

DEFS_Release := \
	'-DV8_TARGET_ARCH_ARM' \
	'-DCAN_USE_ARMV7_INSTRUCTIONS' \
	'-DV8_DEPRECATION_WARNINGS' \
	'-DV8_I18N_SUPPORT' \
	'-DICU_UTIL_DATA_IMPL=ICU_UTIL_DATA_STATIC' \
	'-DU_USING_ICU_NAMESPACE=0' \
	'-DU_ENABLE_DYLOAD=0' \
	'-DU_STATIC_IMPLEMENTATION' \
	'-DUSE_EABI_HARDFLOAT=1' \
	'-DENABLE_HANDLE_ZAPPING'

# Flags passed to all source files.
CFLAGS_Release := \
	-Wall \
	 \
	-W \
	-Wno-unused-parameter \
	-Wno-long-long \
	-pthread \
	-fno-exceptions \
	-pedantic \
	-Wno-missing-field-initializers \
	-m32 \
	-m32 \
	-fdata-sections \
	-ffunction-sections \
	-O3 \
	-fdata-sections \
	-ffunction-sections \
	-O3

# Flags passed to only C files.
CFLAGS_C_Release :=

# Flags passed to only C++ files.
CFLAGS_CC_Release := \
	-Wnon-virtual-dtor \
	-fno-rtti \
	-std=gnu++0x

INCS_Release := \
	-I$(srcdir)/. \
	-I$(srcdir)/third_party/icu/source/i18n \
	-I$(srcdir)/third_party/icu/source/common

OBJS := \
	$(obj).host/$(TARGET)/src/accessors.o \
	$(obj).host/$(TARGET)/src/allocation.o \
	$(obj).host/$(TARGET)/src/allocation-site-scopes.o \
	$(obj).host/$(TARGET)/src/allocation-tracker.o \
	$(obj).host/$(TARGET)/src/api.o \
	$(obj).host/$(TARGET)/src/arguments.o \
	$(obj).host/$(TARGET)/src/assembler.o \
	$(obj).host/$(TARGET)/src/assert-scope.o \
	$(obj).host/$(TARGET)/src/ast-value-factory.o \
	$(obj).host/$(TARGET)/src/ast-numbering.o \
	$(obj).host/$(TARGET)/src/ast.o \
	$(obj).host/$(TARGET)/src/background-parsing-task.o \
	$(obj).host/$(TARGET)/src/bailout-reason.o \
	$(obj).host/$(TARGET)/src/basic-block-profiler.o \
	$(obj).host/$(TARGET)/src/bignum-dtoa.o \
	$(obj).host/$(TARGET)/src/bignum.o \
	$(obj).host/$(TARGET)/src/bit-vector.o \
	$(obj).host/$(TARGET)/src/bootstrapper.o \
	$(obj).host/$(TARGET)/src/builtins.o \
	$(obj).host/$(TARGET)/src/cached-powers.o \
	$(obj).host/$(TARGET)/src/char-predicates.o \
	$(obj).host/$(TARGET)/src/checks.o \
	$(obj).host/$(TARGET)/src/code-factory.o \
	$(obj).host/$(TARGET)/src/code-stubs.o \
	$(obj).host/$(TARGET)/src/code-stubs-hydrogen.o \
	$(obj).host/$(TARGET)/src/codegen.o \
	$(obj).host/$(TARGET)/src/compilation-cache.o \
	$(obj).host/$(TARGET)/src/compilation-statistics.o \
	$(obj).host/$(TARGET)/src/compiler/access-builder.o \
	$(obj).host/$(TARGET)/src/compiler/ast-graph-builder.o \
	$(obj).host/$(TARGET)/src/compiler/ast-loop-assignment-analyzer.o \
	$(obj).host/$(TARGET)/src/compiler/basic-block-instrumentor.o \
	$(obj).host/$(TARGET)/src/compiler/change-lowering.o \
	$(obj).host/$(TARGET)/src/compiler/code-generator.o \
	$(obj).host/$(TARGET)/src/compiler/common-operator.o \
	$(obj).host/$(TARGET)/src/compiler/control-builders.o \
	$(obj).host/$(TARGET)/src/compiler/control-reducer.o \
	$(obj).host/$(TARGET)/src/compiler/gap-resolver.o \
	$(obj).host/$(TARGET)/src/compiler/graph-builder.o \
	$(obj).host/$(TARGET)/src/compiler/graph-reducer.o \
	$(obj).host/$(TARGET)/src/compiler/graph-replay.o \
	$(obj).host/$(TARGET)/src/compiler/graph-visualizer.o \
	$(obj).host/$(TARGET)/src/compiler/graph.o \
	$(obj).host/$(TARGET)/src/compiler/instruction-selector.o \
	$(obj).host/$(TARGET)/src/compiler/instruction.o \
	$(obj).host/$(TARGET)/src/compiler/js-builtin-reducer.o \
	$(obj).host/$(TARGET)/src/compiler/js-context-specialization.o \
	$(obj).host/$(TARGET)/src/compiler/js-generic-lowering.o \
	$(obj).host/$(TARGET)/src/compiler/js-graph.o \
	$(obj).host/$(TARGET)/src/compiler/js-inlining.o \
	$(obj).host/$(TARGET)/src/compiler/js-intrinsic-builder.o \
	$(obj).host/$(TARGET)/src/compiler/js-operator.o \
	$(obj).host/$(TARGET)/src/compiler/js-typed-lowering.o \
	$(obj).host/$(TARGET)/src/compiler/linkage.o \
	$(obj).host/$(TARGET)/src/compiler/machine-operator-reducer.o \
	$(obj).host/$(TARGET)/src/compiler/machine-operator.o \
	$(obj).host/$(TARGET)/src/compiler/machine-type.o \
	$(obj).host/$(TARGET)/src/compiler/node-cache.o \
	$(obj).host/$(TARGET)/src/compiler/node.o \
	$(obj).host/$(TARGET)/src/compiler/operator.o \
	$(obj).host/$(TARGET)/src/compiler/pipeline.o \
	$(obj).host/$(TARGET)/src/compiler/pipeline-statistics.o \
	$(obj).host/$(TARGET)/src/compiler/raw-machine-assembler.o \
	$(obj).host/$(TARGET)/src/compiler/register-allocator.o \
	$(obj).host/$(TARGET)/src/compiler/register-allocator-verifier.o \
	$(obj).host/$(TARGET)/src/compiler/register-configuration.o \
	$(obj).host/$(TARGET)/src/compiler/schedule.o \
	$(obj).host/$(TARGET)/src/compiler/scheduler.o \
	$(obj).host/$(TARGET)/src/compiler/select-lowering.o \
	$(obj).host/$(TARGET)/src/compiler/simplified-lowering.o \
	$(obj).host/$(TARGET)/src/compiler/simplified-operator-reducer.o \
	$(obj).host/$(TARGET)/src/compiler/simplified-operator.o \
	$(obj).host/$(TARGET)/src/compiler/source-position.o \
	$(obj).host/$(TARGET)/src/compiler/typer.o \
	$(obj).host/$(TARGET)/src/compiler/value-numbering-reducer.o \
	$(obj).host/$(TARGET)/src/compiler/verifier.o \
	$(obj).host/$(TARGET)/src/compiler/zone-pool.o \
	$(obj).host/$(TARGET)/src/compiler.o \
	$(obj).host/$(TARGET)/src/contexts.o \
	$(obj).host/$(TARGET)/src/conversions.o \
	$(obj).host/$(TARGET)/src/counters.o \
	$(obj).host/$(TARGET)/src/cpu-profiler.o \
	$(obj).host/$(TARGET)/src/date.o \
	$(obj).host/$(TARGET)/src/dateparser.o \
	$(obj).host/$(TARGET)/src/debug.o \
	$(obj).host/$(TARGET)/src/deoptimizer.o \
	$(obj).host/$(TARGET)/src/disassembler.o \
	$(obj).host/$(TARGET)/src/diy-fp.o \
	$(obj).host/$(TARGET)/src/dtoa.o \
	$(obj).host/$(TARGET)/src/elements-kind.o \
	$(obj).host/$(TARGET)/src/elements.o \
	$(obj).host/$(TARGET)/src/execution.o \
	$(obj).host/$(TARGET)/src/extensions/externalize-string-extension.o \
	$(obj).host/$(TARGET)/src/extensions/free-buffer-extension.o \
	$(obj).host/$(TARGET)/src/extensions/gc-extension.o \
	$(obj).host/$(TARGET)/src/extensions/statistics-extension.o \
	$(obj).host/$(TARGET)/src/extensions/trigger-failure-extension.o \
	$(obj).host/$(TARGET)/src/factory.o \
	$(obj).host/$(TARGET)/src/fast-dtoa.o \
	$(obj).host/$(TARGET)/src/fixed-dtoa.o \
	$(obj).host/$(TARGET)/src/flags.o \
	$(obj).host/$(TARGET)/src/frames.o \
	$(obj).host/$(TARGET)/src/full-codegen.o \
	$(obj).host/$(TARGET)/src/func-name-inferrer.o \
	$(obj).host/$(TARGET)/src/gdb-jit.o \
	$(obj).host/$(TARGET)/src/global-handles.o \
	$(obj).host/$(TARGET)/src/handles.o \
	$(obj).host/$(TARGET)/src/heap-profiler.o \
	$(obj).host/$(TARGET)/src/heap-snapshot-generator.o \
	$(obj).host/$(TARGET)/src/heap/gc-idle-time-handler.o \
	$(obj).host/$(TARGET)/src/heap/gc-tracer.o \
	$(obj).host/$(TARGET)/src/heap/heap.o \
	$(obj).host/$(TARGET)/src/heap/incremental-marking.o \
	$(obj).host/$(TARGET)/src/heap/mark-compact.o \
	$(obj).host/$(TARGET)/src/heap/objects-visiting.o \
	$(obj).host/$(TARGET)/src/heap/spaces.o \
	$(obj).host/$(TARGET)/src/heap/store-buffer.o \
	$(obj).host/$(TARGET)/src/hydrogen-bce.o \
	$(obj).host/$(TARGET)/src/hydrogen-bch.o \
	$(obj).host/$(TARGET)/src/hydrogen-canonicalize.o \
	$(obj).host/$(TARGET)/src/hydrogen-check-elimination.o \
	$(obj).host/$(TARGET)/src/hydrogen-dce.o \
	$(obj).host/$(TARGET)/src/hydrogen-dehoist.o \
	$(obj).host/$(TARGET)/src/hydrogen-environment-liveness.o \
	$(obj).host/$(TARGET)/src/hydrogen-escape-analysis.o \
	$(obj).host/$(TARGET)/src/hydrogen-instructions.o \
	$(obj).host/$(TARGET)/src/hydrogen.o \
	$(obj).host/$(TARGET)/src/hydrogen-gvn.o \
	$(obj).host/$(TARGET)/src/hydrogen-infer-representation.o \
	$(obj).host/$(TARGET)/src/hydrogen-infer-types.o \
	$(obj).host/$(TARGET)/src/hydrogen-load-elimination.o \
	$(obj).host/$(TARGET)/src/hydrogen-mark-deoptimize.o \
	$(obj).host/$(TARGET)/src/hydrogen-mark-unreachable.o \
	$(obj).host/$(TARGET)/src/hydrogen-osr.o \
	$(obj).host/$(TARGET)/src/hydrogen-range-analysis.o \
	$(obj).host/$(TARGET)/src/hydrogen-redundant-phi.o \
	$(obj).host/$(TARGET)/src/hydrogen-removable-simulates.o \
	$(obj).host/$(TARGET)/src/hydrogen-representation-changes.o \
	$(obj).host/$(TARGET)/src/hydrogen-sce.o \
	$(obj).host/$(TARGET)/src/hydrogen-store-elimination.o \
	$(obj).host/$(TARGET)/src/hydrogen-types.o \
	$(obj).host/$(TARGET)/src/hydrogen-uint32-analysis.o \
	$(obj).host/$(TARGET)/src/i18n.o \
	$(obj).host/$(TARGET)/src/icu_util.o \
	$(obj).host/$(TARGET)/src/ic/access-compiler.o \
	$(obj).host/$(TARGET)/src/ic/call-optimization.o \
	$(obj).host/$(TARGET)/src/ic/handler-compiler.o \
	$(obj).host/$(TARGET)/src/ic/ic-state.o \
	$(obj).host/$(TARGET)/src/ic/ic.o \
	$(obj).host/$(TARGET)/src/ic/ic-compiler.o \
	$(obj).host/$(TARGET)/src/interface.o \
	$(obj).host/$(TARGET)/src/interface-descriptors.o \
	$(obj).host/$(TARGET)/src/interpreter-irregexp.o \
	$(obj).host/$(TARGET)/src/isolate.o \
	$(obj).host/$(TARGET)/src/jsregexp.o \
	$(obj).host/$(TARGET)/src/layout-descriptor.o \
	$(obj).host/$(TARGET)/src/lithium-allocator.o \
	$(obj).host/$(TARGET)/src/lithium-codegen.o \
	$(obj).host/$(TARGET)/src/lithium.o \
	$(obj).host/$(TARGET)/src/liveedit.o \
	$(obj).host/$(TARGET)/src/log-utils.o \
	$(obj).host/$(TARGET)/src/log.o \
	$(obj).host/$(TARGET)/src/lookup.o \
	$(obj).host/$(TARGET)/src/messages.o \
	$(obj).host/$(TARGET)/src/objects-debug.o \
	$(obj).host/$(TARGET)/src/objects-printer.o \
	$(obj).host/$(TARGET)/src/objects.o \
	$(obj).host/$(TARGET)/src/optimizing-compiler-thread.o \
	$(obj).host/$(TARGET)/src/ostreams.o \
	$(obj).host/$(TARGET)/src/parser.o \
	$(obj).host/$(TARGET)/src/perf-jit.o \
	$(obj).host/$(TARGET)/src/preparse-data.o \
	$(obj).host/$(TARGET)/src/preparser.o \
	$(obj).host/$(TARGET)/src/prettyprinter.o \
	$(obj).host/$(TARGET)/src/profile-generator.o \
	$(obj).host/$(TARGET)/src/property.o \
	$(obj).host/$(TARGET)/src/regexp-macro-assembler-irregexp.o \
	$(obj).host/$(TARGET)/src/regexp-macro-assembler-tracer.o \
	$(obj).host/$(TARGET)/src/regexp-macro-assembler.o \
	$(obj).host/$(TARGET)/src/regexp-stack.o \
	$(obj).host/$(TARGET)/src/rewriter.o \
	$(obj).host/$(TARGET)/src/runtime-profiler.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-api.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-array.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-classes.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-collections.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-compiler.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-date.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-debug.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-function.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-generator.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-i18n.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-internal.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-json.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-literals.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-liveedit.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-maths.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-numbers.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-object.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-observe.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-proxy.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-regexp.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-scopes.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-strings.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-symbol.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-test.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-typedarray.o \
	$(obj).host/$(TARGET)/src/runtime/runtime-uri.o \
	$(obj).host/$(TARGET)/src/runtime/runtime.o \
	$(obj).host/$(TARGET)/src/safepoint-table.o \
	$(obj).host/$(TARGET)/src/sampler.o \
	$(obj).host/$(TARGET)/src/scanner-character-streams.o \
	$(obj).host/$(TARGET)/src/scanner.o \
	$(obj).host/$(TARGET)/src/scopeinfo.o \
	$(obj).host/$(TARGET)/src/scopes.o \
	$(obj).host/$(TARGET)/src/serialize.o \
	$(obj).host/$(TARGET)/src/snapshot-source-sink.o \
	$(obj).host/$(TARGET)/src/string-builder.o \
	$(obj).host/$(TARGET)/src/string-search.o \
	$(obj).host/$(TARGET)/src/string-stream.o \
	$(obj).host/$(TARGET)/src/strtod.o \
	$(obj).host/$(TARGET)/src/ic/stub-cache.o \
	$(obj).host/$(TARGET)/src/token.o \
	$(obj).host/$(TARGET)/src/transitions.o \
	$(obj).host/$(TARGET)/src/type-feedback-vector.o \
	$(obj).host/$(TARGET)/src/type-info.o \
	$(obj).host/$(TARGET)/src/types.o \
	$(obj).host/$(TARGET)/src/typing.o \
	$(obj).host/$(TARGET)/src/unicode.o \
	$(obj).host/$(TARGET)/src/unicode-decoder.o \
	$(obj).host/$(TARGET)/src/utils.o \
	$(obj).host/$(TARGET)/src/v8.o \
	$(obj).host/$(TARGET)/src/v8threads.o \
	$(obj).host/$(TARGET)/src/variables.o \
	$(obj).host/$(TARGET)/src/version.o \
	$(obj).host/$(TARGET)/src/zone.o \
	$(obj).host/$(TARGET)/src/third_party/fdlibm/fdlibm.o \
	$(obj).host/$(TARGET)/src/arm/assembler-arm.o \
	$(obj).host/$(TARGET)/src/arm/builtins-arm.o \
	$(obj).host/$(TARGET)/src/arm/code-stubs-arm.o \
	$(obj).host/$(TARGET)/src/arm/codegen-arm.o \
	$(obj).host/$(TARGET)/src/arm/constants-arm.o \
	$(obj).host/$(TARGET)/src/arm/cpu-arm.o \
	$(obj).host/$(TARGET)/src/arm/debug-arm.o \
	$(obj).host/$(TARGET)/src/arm/deoptimizer-arm.o \
	$(obj).host/$(TARGET)/src/arm/disasm-arm.o \
	$(obj).host/$(TARGET)/src/arm/frames-arm.o \
	$(obj).host/$(TARGET)/src/arm/full-codegen-arm.o \
	$(obj).host/$(TARGET)/src/arm/interface-descriptors-arm.o \
	$(obj).host/$(TARGET)/src/arm/lithium-arm.o \
	$(obj).host/$(TARGET)/src/arm/lithium-codegen-arm.o \
	$(obj).host/$(TARGET)/src/arm/lithium-gap-resolver-arm.o \
	$(obj).host/$(TARGET)/src/arm/macro-assembler-arm.o \
	$(obj).host/$(TARGET)/src/arm/regexp-macro-assembler-arm.o \
	$(obj).host/$(TARGET)/src/arm/simulator-arm.o \
	$(obj).host/$(TARGET)/src/compiler/arm/code-generator-arm.o \
	$(obj).host/$(TARGET)/src/compiler/arm/instruction-selector-arm.o \
	$(obj).host/$(TARGET)/src/compiler/arm/linkage-arm.o \
	$(obj).host/$(TARGET)/src/ic/arm/access-compiler-arm.o \
	$(obj).host/$(TARGET)/src/ic/arm/handler-compiler-arm.o \
	$(obj).host/$(TARGET)/src/ic/arm/ic-arm.o \
	$(obj).host/$(TARGET)/src/ic/arm/ic-compiler-arm.o \
	$(obj).host/$(TARGET)/src/ic/arm/stub-cache-arm.o

# Add to the list of files we specially track dependencies for.
all_deps += $(OBJS)

# CFLAGS et al overrides must be target-local.
# See "Target-specific Variable Values" in the GNU Make manual.
$(OBJS): TOOLSET := $(TOOLSET)
$(OBJS): GYP_CFLAGS := $(DEFS_$(BUILDTYPE)) $(INCS_$(BUILDTYPE))  $(CFLAGS_$(BUILDTYPE)) $(CFLAGS_C_$(BUILDTYPE))
$(OBJS): GYP_CXXFLAGS := $(DEFS_$(BUILDTYPE)) $(INCS_$(BUILDTYPE))  $(CFLAGS_$(BUILDTYPE)) $(CFLAGS_CC_$(BUILDTYPE))

# Suffix rules, putting all outputs into $(obj).

$(obj).$(TOOLSET)/$(TARGET)/%.o: $(srcdir)/%.cc FORCE_DO_CMD
	@$(call do_cmd,cxx,1)

# Try building from generated source, too.

$(obj).$(TOOLSET)/$(TARGET)/%.o: $(obj).$(TOOLSET)/%.cc FORCE_DO_CMD
	@$(call do_cmd,cxx,1)

$(obj).$(TOOLSET)/$(TARGET)/%.o: $(obj)/%.cc FORCE_DO_CMD
	@$(call do_cmd,cxx,1)

# End of this set of suffix rules
### Rules for final target.
LDFLAGS_Debug := \
	-pthread \
	-m32 \
	-m32 \
	-rdynamic \
	-rdynamic

LDFLAGS_Optdebug := \
	-pthread \
	-m32 \
	-m32 \
	-rdynamic \
	-rdynamic

LDFLAGS_Release := \
	-pthread \
	-m32 \
	-m32

LIBS :=

$(obj).host/tools/gyp/libv8_base.a: GYP_LDFLAGS := $(LDFLAGS_$(BUILDTYPE))
$(obj).host/tools/gyp/libv8_base.a: LIBS := $(LIBS)
$(obj).host/tools/gyp/libv8_base.a: TOOLSET := $(TOOLSET)
$(obj).host/tools/gyp/libv8_base.a: $(OBJS) FORCE_DO_CMD
	$(call do_cmd,alink_thin)

all_deps += $(obj).host/tools/gyp/libv8_base.a
# Add target alias
.PHONY: v8_base
v8_base: $(obj).host/tools/gyp/libv8_base.a

# Add target alias to "all" target.
.PHONY: all
all: v8_base

